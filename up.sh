#!/usr/bin/env bash

tmux new-session \
  \; send 'docker-compose run --rm tailscale' ENTER \
  \; split-window -v \
  \; send 'until nc -z 127.0.0.1 10055 ; do echo "waiting for socks port" && sleep 1 ; done ; sleep 1 ; echo "socks available on 127.0.0.1:10055"' ENTER \
  \; attach
