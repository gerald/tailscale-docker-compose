# Expose tailnet through Socks Proxy in container

- Requires tmux + docker-compose

## Example SSH config

```
Host host-in-tailnet
  Hostname 100.110.x.x # Tailnet IP
  ProxyCommand /usr/bin/nc -X 5 -x 127.0.0.1:10055 %h %p
```
